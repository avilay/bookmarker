CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FacebookId] [bigint] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[AccessToken] [nvarchar](max) NULL,
	[Expires] [datetime] NOT NULL,
	PRIMARY KEY CLUSTERED (	[Id] ASC )
)
GO

/****** Object:  Table [dbo].[Bookmarks]    Script Date: 10/13/2011 15:12:06 ******/
CREATE TABLE [dbo].[Bookmarks](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Url] [nvarchar](max) NOT NULL,
	[ContentType] [nvarchar](max) NULL,
	[LastUpdatedOn] [datetime] NOT NULL,
	[Content] [nvarchar](max) NULL,
	[ContentHash] [int] NOT NULL,
	PRIMARY KEY CLUSTERED (	[Id] ASC )
)
GO

/****** Object:  Table [dbo].[EdmMetadata]    Script Date: 10/13/2011 15:12:06 ******/
CREATE TABLE [dbo].[EdmMetadata](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ModelHash] [nvarchar](max) NULL,
	PRIMARY KEY CLUSTERED (	[Id] ASC )
)
GO

/****** Object:  Table [dbo].[Clusters]    Script Date: 10/13/2011 15:12:06 ******/
CREATE TABLE [dbo].[Clusters](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UserId] [int] NOT NULL,
	PRIMARY KEY CLUSTERED (	[Id] ASC )
)
GO

/****** Object:  Table [dbo].[Favorites]    Script Date: 10/13/2011 15:12:06 ******/
CREATE TABLE [dbo].[Favorites](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Notes] [nvarchar](max) NULL,
	[BookmarkId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[ClusterId] [int] NULL,
	PRIMARY KEY CLUSTERED (	[Id] ASC )
)
GO

/****** Object:  ForeignKey [Cluster_User]    Script Date: 10/13/2011 15:12:06 ******/
ALTER TABLE [dbo].[Clusters]  WITH CHECK ADD  CONSTRAINT [Cluster_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Clusters] CHECK CONSTRAINT [Cluster_User]
GO

/****** Object:  ForeignKey [Cluster_Favorites]    Script Date: 10/13/2011 15:12:06 ******/
ALTER TABLE [dbo].[Favorites]  WITH CHECK ADD  CONSTRAINT [Cluster_Favorites] FOREIGN KEY([ClusterId])
REFERENCES [dbo].[Clusters] ([Id])
GO
ALTER TABLE [dbo].[Favorites] CHECK CONSTRAINT [Cluster_Favorites]
GO

/****** Object:  ForeignKey [Favorite_Bookmark]    Script Date: 10/13/2011 15:12:06 ******/
ALTER TABLE [dbo].[Favorites]  WITH CHECK ADD  CONSTRAINT [Favorite_Bookmark] FOREIGN KEY([BookmarkId])
REFERENCES [dbo].[Bookmarks] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Favorites] CHECK CONSTRAINT [Favorite_Bookmark]
GO

/****** Object:  ForeignKey [User_Favorites]    Script Date: 10/13/2011 15:12:06 ******/
ALTER TABLE [dbo].[Favorites]  WITH CHECK ADD  CONSTRAINT [User_Favorites] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Favorites] CHECK CONSTRAINT [User_Favorites]
GO
