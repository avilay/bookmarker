﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BookmarkerDataModel;
using System.Data.Entity;
using HtmlAgilityPack;
using System.Net;
using System.Text.RegularExpressions;
using TextAnalysis;

namespace Tools
{
    public class Program
    {
        public static BookmarkerContext _db;
        public static DocumentFactory _df;

        static void Main(string[] args) {
            _db = new BookmarkerContext();
            _df = new DocumentFactory();
            if (args[0] == "-cleandb") CleanDb();
            if (args[0] == "-crawl") CrawlAll();
            //if (args[0] == "-cluster") Clusterer.ClusterAll();
            if (args[0] == "-scratch") Scratch();
        }

        static void Scratch() {
            foreach (Bookmark bm in _db.Bookmarks) {
                bm.Title = Regex.Replace(bm.Title, "\\s+", " ").Trim();
            }
            _db.SaveChanges();
            foreach (Bookmark bm in _db.Bookmarks) {
                Console.WriteLine(bm.Title);
            }
        }

        static void CleanDb() {
            Database.SetInitializer<BookmarkerContext>(new BookmarkerInitializer());
            BookmarkerContext db = new BookmarkerContext();
            int total = db.Bookmarks.Count();
            Console.WriteLine("Bookmarks created with {0} bookmarks", total);
            Console.WriteLine("Remember to add NT AUTHORITY\\NETWORK SERVICE to Bookmarker");
        }

        static void CrawlAll() {            
            Console.WriteLine("Starting crawl");
            List<Bookmark> bookmarks = _db.Bookmarks.ToList();
            foreach (Bookmark bookmark in bookmarks) {
                try {
                    Crawl(bookmark);
                    _db.SaveChanges();
                    Console.WriteLine("Added " + bookmark.Id + ". " + bookmark.Title);
                }
                catch (Exception ex) {
                    Console.WriteLine("Could not crawl " + bookmark.Url);
                    Console.WriteLine(ex.GetType().FullName + " " + ex.Message);
                }
            }
            Console.WriteLine("Ending crawl");
        }

        public static void Crawl(Bookmark bookmark) {
            string contentType;
            HtmlDocument doc = Download(bookmark.Url, out contentType);
            string title = StripTitle(doc);
            title = Regex.Replace(title, "\\s+", " ").Trim();
            string text = StripText(doc);

            bookmark.Title = title != null ? title : bookmark.Title;
            bookmark.ContentType = contentType;
            bookmark.Content = text;
            bookmark.ContentHash = text.GetHashCode();
            bookmark.LastUpdatedOn = DateTime.UtcNow;
            AddToVocabulary(bookmark);
        }

        private static void AddToVocabulary(Bookmark bookmark) {
            Document document = _df.CreateDocument(bookmark.Content);
            foreach (string word in document.Vocabulary) {
                string word1 = word.ToLowerInvariant();
                Vocabulary dbWord = _db.Words.FirstOrDefault(w => w.Word == word1);
                if (dbWord == null) {
                    List<Bookmark> bms = new List<Bookmark>();
                    bms.Add(bookmark);
                    _db.Words.Add(new Vocabulary { Word = word1, Bookmarks = bms });
                }
                else {
                    dbWord.Bookmarks.Add(bookmark);
                }
            }
        }

        private static HtmlDocument Download(string bookmarkUrl, out string contentType) {
            HtmlDocument doc = new HtmlDocument();
            WebRequest req = WebRequest.Create(bookmarkUrl);
            using (WebResponse res = req.GetResponse()) {
                contentType = res.ContentType;
                doc.Load(res.GetResponseStream());
            }
            return doc;
        }

        private static string StripText(HtmlDocument doc) {
            List<HtmlTextNode> textNodes = new List<HtmlTextNode>();
            GetTextNodes(doc.DocumentNode, textNodes);
            string text = "";
            foreach (HtmlTextNode textNode in textNodes) {
                if (!string.IsNullOrEmpty(textNode.InnerText) && !string.IsNullOrWhiteSpace(textNode.InnerText)) {
                    string tempTxt = textNode.InnerText.Trim();
                    if (Regex.IsMatch(tempTxt, "http://.*") || Regex.IsMatch(tempTxt, "<[^>]+>")) continue;
                    text += textNode.InnerText.Trim() + " ";
                }
            }
            return WebUtility.HtmlDecode(text);
        }

        private static string StripTitle(HtmlDocument doc) {
            string title = null;
            HtmlNode titleNode = doc.DocumentNode.SelectSingleNode("/html/head/title");
            if (titleNode != null) {
                title = titleNode.InnerText;
            }
            return title;
        }

        public static void GetTextNodes(HtmlNode root, List<HtmlTextNode> textNodes) {
            if (root.NodeType == HtmlNodeType.Text) {
                textNodes.Add((HtmlTextNode)root);
            }
            foreach (HtmlNode child in root.ChildNodes) {
                if (child.Name != "script" && child.Name != "style")
                    GetTextNodes(child, textNodes);
            }
        }
    }
}
