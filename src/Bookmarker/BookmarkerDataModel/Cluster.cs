﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BookmarkerDataModel
{
    public class Cluster
    {
        public int Id { get; set; }

        [Required(ErrorMessage="Cluster must have a name")]
        public string Name { get; set; }

        public DateTime CreatedOn { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<Favorite> Favorites { get; set; }        
    }
}