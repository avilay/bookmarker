﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Json;

namespace BookmarkerDataModel
{
    public class ImportList
    {
        public int UserId { get; set; }
        public string BlobName { get; set; }

        public string ToJsonString() {
            string importList = "";
            var serializer = new DataContractJsonSerializer(this.GetType());
            using (var ms = new MemoryStream()) {
                serializer.WriteObject(ms, this);
                importList = Encoding.Default.GetString(ms.ToArray());
            }
            return importList;
        }

        public static ImportList Deserialize(string json) {
            ImportList toImport = null;
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(json))) {
                var serialiser = new DataContractJsonSerializer(typeof(ImportList));
                toImport = (ImportList)serialiser.ReadObject(ms);
            }
            return toImport;
        }
    }
}
