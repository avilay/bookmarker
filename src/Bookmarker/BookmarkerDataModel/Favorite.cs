﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BookmarkerDataModel
{
    public class Favorite
    {
        public int Id { get; set; }
        
        [Display(Name="Added On")]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime CreatedOn { get; set; }
        
        public string Name { get; set; }

        public string Notes { get; set; }

        public int BookmarkId { get; set; }
        public virtual Bookmark Bookmark { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public bool IsPinned { get; set; }

        public int? ClusterId { get; set; }

        [Display(Name="Group")]
        public virtual Cluster Cluster { get; set; }
    }
}