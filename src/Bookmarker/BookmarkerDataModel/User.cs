﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookmarkerDataModel
{
    public class User
    {
        public int Id { get; set; }
        public string FacebookId { get; set; }
        public string Name { get; set; }
        public string AccessToken { get; set; }
        public DateTime Expires { get; set; }
        public DateTime LastActivityOn { get; set; }

        public virtual ICollection<Favorite> Favorites { get; set; }
        public virtual ICollection<Cluster> Clusters { get; set; }
    }
}