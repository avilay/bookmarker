﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BookmarkerDataModel
{
    public class BookmarkerInitializer : DropCreateDatabaseAlways<BookmarkerContext>
    {
        protected override void Seed(BookmarkerContext context) {
            List<User> users = new List<User> {
                new User { FacebookId = "1", Name = "Happy Orange", AccessToken = "some token", Expires = DateTime.Now, LastActivityOn = DateTime.Now },
                new User { FacebookId = "2", Name = "Cookie Monster", AccessToken = "token duo", Expires = DateTime.Now, LastActivityOn = DateTime.Now }
            };
            users.ForEach(u => context.Users.Add(u));
            context.SaveChanges();
            
            List<Bookmark> bookmarks = new List<Bookmark> {
                new Bookmark { 
                    Title = "Microsoft Home Page",
                    Url = "http://www.microsoft.com", 
                    LastUpdatedOn = DateTime.Now, 
                    Content = "content from microsoft.com", 
                    ContentHash = "content from microsoft.com".GetHashCode() 
                },
                new Bookmark { 
                    Title = "Google Home Page",        
                    Url = "http://www.google.com", 
                    LastUpdatedOn = DateTime.Now, 
                    Content = "content from google.com", 
                    ContentHash = "content from google.com".GetHashCode() 
                },
                new Bookmark {
                    Title = "New York Times Blog",
                    Url = "http://www.nytimes.com/services/xml/rss/nyt/HomePage.xml", 
                    LastUpdatedOn = DateTime.Now, 
                    Content = "content from nytimes", 
                    ContentHash = "content from nytimes".GetHashCode() 
                }
            };
            bookmarks.ForEach(b => context.Bookmarks.Add(b));
            context.SaveChanges();

            List<Cluster> clusters = new List<Cluster> {
                new Cluster { Name = "technology", CreatedOn = DateTime.Now, UserId = users[0].Id },
                new Cluster { Name = "blogs", CreatedOn = DateTime.Now, UserId = users[0].Id },
                new Cluster { Name = "design", CreatedOn = DateTime.Now, UserId = users[0].Id }
            };
            clusters.ForEach(c => context.Clusters.Add(c));
            context.SaveChanges();
            
            List<Favorite> favorites = new List<Favorite> {
                new Favorite { 
                    BookmarkId = bookmarks[0].Id, 
                    ClusterId = clusters[0].Id, 
                    CreatedOn = DateTime.Now, 
                    Name = "Home Page MS",
                    Notes = "some notes on microsoft", 
                    IsPinned = true,
                    UserId = users[0].Id 
                },
                new Favorite { 
                    BookmarkId = bookmarks[1].Id, 
                    ClusterId = clusters[0].Id, 
                    CreatedOn = DateTime.Now, 
                    Name = "GOOG home",
                    IsPinned = true,
                    Notes = "some notes on google", 
                    UserId = users[0].Id 
                },
                new Favorite { 
                    BookmarkId = bookmarks[2].Id, 
                    ClusterId = clusters[1].Id, 
                    Name = "NYT Home Page",
                    CreatedOn = DateTime.Now, 
                    Notes = "some notes on nytimes", 
                    UserId = users[0].Id 
                },
                new Favorite { 
                    BookmarkId = bookmarks[2].Id, 
                    Name = bookmarks[2].Title,
                    CreatedOn = DateTime.Now, 
                    UserId = users[1].Id 
                },
            };
            favorites.ForEach(f => context.Favorites.Add(f));
            context.SaveChanges();
        }
    }
}