﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BookmarkerDataModel
{
    public class UrlFormatAttribute : ValidationAttribute
    {
        public override bool IsValid(object value) {
            if ( value == null ) return true;
            string url = (string)value;
            Uri tmp;
            if ( Uri.TryCreate(url, UriKind.Absolute, out tmp) ) {
                return true;
            }
            else {
                return false;
            }
        }
    }
}