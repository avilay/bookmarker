﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BookmarkerDataModel
{
    public class BookmarkInfo
    {
        public int BookmarkId { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public int Relevance { get; set; }
    }
}
