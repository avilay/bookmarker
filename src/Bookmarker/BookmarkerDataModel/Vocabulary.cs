﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BookmarkerDataModel
{
    public class Vocabulary
    {
        public int Id { get; set; }
        public string Word { get; set; }

        public virtual ICollection<Bookmark> Bookmarks { get; set; }
    }
}
