﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using HtmlAgilityPack;
using System.Net;
using System.IO;

namespace BookmarkerDataModel
{
    public class Bookmark
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public string Cookie { get; set; }

        [Required(ErrorMessage="Bookmark must have a valid URL")]
        [UrlFormat(ErrorMessage="Bookmark must have URL in a valid format like http://someuri.com")]
        public string Url { get; set; }

        public string ContentType { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public string Content { get; set; }
        public int ContentHash { get; set; }

        //public string Cookie { get; set; }
        
        public virtual ICollection<Favorite> Favorites { get; set; }

        public virtual ICollection<Vocabulary> Words { get; set; }
        
        public Bookmark Load(BookmarkerContext db) {
            Uri tmp;
            if ( Uri.TryCreate(Url, UriKind.Absolute, out tmp) ) {
                var bookmarks = from b in db.Bookmarks
                                where b.Url == Url
                                select b;
                if ( bookmarks.Count() == 0 ) {
                    if ( string.IsNullOrEmpty(Title) ) {
                        Title = tmp.Host;
                    }
                    LastUpdatedOn = DateTime.Now;
                    ContentHash = 0;
                    db.Bookmarks.Add(this);
                    db.SaveChanges();
                    return this;
                }
                else if ( bookmarks.Count() == 1 ) {
                    return bookmarks.First();
                }
                else {
                    throw new Exception("There are multiple entries in the db for url " + Url);
                }
            }
            else {
                throw new Exception("Invalid URI format for " + Url);
            }
        }

        public void RefreshContent() {
            WebRequest req = WebRequest.Create(Url);
            using ( WebResponse res = req.GetResponse() ) {
                ContentType = res.ContentType;
                HtmlDocument doc = new HtmlDocument();
                doc.Load(res.GetResponseStream());
                HtmlNode titleNode = doc.DocumentNode.SelectSingleNode("/html/head/title");
                if ( titleNode != null ) {
                    Title = titleNode.InnerText;
                }
                List<HtmlTextNode> textNodes = new List<HtmlTextNode>();
                GetTextNodes(doc.DocumentNode, textNodes);
                string text = "";
                foreach ( HtmlTextNode textNode in textNodes ) {
                    if ( !string.IsNullOrEmpty(textNode.InnerText) && !string.IsNullOrWhiteSpace(textNode.InnerText) ) {
                        text += textNode.InnerText.Trim() + " ";
                    }
                }
                if ( ContentHash == text.GetHashCode() ) {
                    // Log that the URL did not change
                }
                Content = WebUtility.HtmlDecode(text);
                ContentHash = Content.GetHashCode();
                LastUpdatedOn = DateTime.UtcNow;
            }
        }

        public void GetTextNodes(HtmlNode root, List<HtmlTextNode> textNodes) {
            if ( root.NodeType == HtmlNodeType.Text ) {
                textNodes.Add((HtmlTextNode)root);
            }
            foreach ( HtmlNode child in root.ChildNodes ) {
                if ( child.Name != "script" && child.Name != "style" )
                    GetTextNodes(child, textNodes);
            }
        }
        
    }
}