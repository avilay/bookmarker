﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BookmarkerDataModel
{
    public class BookmarkerContext : DbContext
    {
        public DbSet<Bookmark> Bookmarks { get; set; }
        public DbSet<Favorite> Favorites { get; set; }
        public DbSet<Cluster> Clusters { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Vocabulary> Words { get; set; }
    }
}