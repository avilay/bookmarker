﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookmarkerDataModel;
using Microsoft.WindowsAzure.Diagnostics;
using System.Diagnostics;
using TextAnalysis;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace SearchService.Controllers
{
    public class SearchController : Controller
    {
        private BookmarkerContext _db = new BookmarkerContext();
        private DocumentFactory _df = new DocumentFactory();

        public JsonResult Index(int userId, string query) {
            Trace.WriteLine("Inside search: searching for " + query);
            Dictionary<int, int> bookmarks = FindAllBookmarks(query);
            List<BookmarkInfo> searchResults = FilterForUser(userId, bookmarks);
            return Json(searchResults, JsonRequestBehavior.AllowGet);
        }

        private List<BookmarkInfo> FilterForUser(int userId, Dictionary<int, int> bookmarks) {
            List<BookmarkInfo> searchResults = new List<BookmarkInfo>();
            foreach (Favorite fav in _db.Users.Find(userId).Favorites) {
                if (bookmarks.ContainsKey(fav.BookmarkId)) {
                    BookmarkInfo bi = new BookmarkInfo {
                        BookmarkId = fav.BookmarkId,
                        Name = fav.Bookmark.Title,
                        Notes = fav.Notes,
                        Url = fav.Bookmark.Url,
                        Relevance = bookmarks[fav.BookmarkId]
                    };
                    searchResults.Add(bi);
                }
            }
            return searchResults;
        }

        private Dictionary<int, int> FindAllBookmarks(string query) {
            Document queryDoc = _df.CreateDocument(query);
            Dictionary<int, int> bookmarks = new Dictionary<int, int>();
            foreach (string queryWord in queryDoc.Vocabulary) {
                Vocabulary v = _db.Words.FirstOrDefault(w => w.Word == queryWord);
                if (v != null) {
                    foreach (Bookmark bm in v.Bookmarks) {
                        int count = 0;
                        bookmarks.TryGetValue(bm.Id, out count);
                        count++;
                        bookmarks[bm.Id] = count;
                    }
                }
            }
            return bookmarks;
        }

    }
}
