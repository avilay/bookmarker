using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Diagnostics;

namespace SearchService
{
    public class WebRole : RoleEntryPoint
    {
        private void InitializeDiagnosticsLogs() {
            var config = DiagnosticMonitor.GetDefaultInitialConfiguration();
            config.Logs.ScheduledTransferPeriod = TimeSpan.FromMinutes(3.0);
            //config.Logs.BufferQuotaInMB = 1024;
            DiagnosticMonitor.Start("Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString", config);
        }

        public override bool OnStart() {
            InitializeDiagnosticsLogs();
            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.

            //foreach (var role in RoleEnvironment.Roles) {
            //    Trace.WriteLine(role.Key + ": " + role.Value.Name);
            //    Trace.WriteLine("Role instances :-");
            //    foreach (var roleInstance in role.Value.Instances) {
            //        Trace.WriteLine("Role instance ID: " + roleInstance.Id);
            //        Trace.WriteLine("Role instance FD: " + roleInstance.FaultDomain);
            //        Trace.WriteLine("Role instance UD: " + roleInstance.UpdateDomain);
            //        Trace.WriteLine("Role instance endpoints :-");
            //        foreach (var instanceEndpoint in roleInstance.InstanceEndpoints) {
            //            Trace.WriteLine(instanceEndpoint.Key + ": " + instanceEndpoint.Value.IPEndpoint.ToString());
            //        }
            //    }
            //}

            return base.OnStart();
        }
    }
}
