﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.StorageClient;
using Avilay.Utils.Logging;
using System.Configuration;
using HtmlAgilityPack;
using System.IO;

namespace Crawler
{
    public class WorkerRole : RoleEntryPoint
    {
        public override void Run() {
            // This is a sample worker implementation. Replace with your logic.
            //Trace.WriteLine("Crawler entry point called", "Information");
            LogFunctions.Info("Inside Crawler.Run", GetServiceLogContext());
            int ctr = 0;
            while ( true ) {
                ctr++;
                if ( ctr > 1 ) {
                    LogFunctions.Debug("Skipping loop", GetServiceLogContext());
                    continue;       
                }
                Thread.Sleep(10000);
                string Title = "";
                string Content = "";
                string ContentType = "";
                WebRequest req = WebRequest.Create("http://www.yahoo.com");
                //to set the header have to use HttpWebRequet.UserAgent property.
                using ( WebResponse res = req.GetResponse() ) {
                    LogFunctions.Debug("Got successful response", GetServiceLogContext());
                    ContentType = res.ContentType;
                    if ( ContentType.StartsWith("text/html") ) {
                        HtmlDocument doc = new HtmlDocument();
                        doc.Load(res.GetResponseStream());
                        HtmlNode titleNode = doc.DocumentNode.SelectSingleNode("/html/head/title");
                        if ( titleNode != null ) {
                            Title = titleNode.InnerText;                            
                        }
                        StringWriter writer = new StringWriter();
                        doc.Save(writer);
                        Content = writer.ToString();
                    }
                    else if ( ContentType.StartsWith("text") || ContentType.Contains("xml") ) {
                        using ( StreamReader reader = new StreamReader(res.GetResponseStream()) ) {
                            Content = reader.ReadToEnd();
                        }
                    }
                    else {
                        Content = string.Empty;
                    }
                    string msg = string.Format("Title: {0}, ContentType: {1}, Content: {2}", Title, ContentType, Content);
                    LogFunctions.Debug(msg, GetServiceLogContext());
                }
                
            }
        }

        public ServiceLogContext GetServiceLogContext() {
            ServiceLogContext tmpContext = new ServiceLogContext {
                ServiceName = "Crawler",
                ComponentName = "",
                RequestId = Guid.Empty,
                SessionId = Guid.Empty
            };
            return tmpContext;
        }

        public override bool OnStart() {
            // Set the maximum number of concurrent connections 
            ServicePointManager.DefaultConnectionLimit = 12;

            SqlLogger logger = new SqlLogger();
            logger.Initialize(ConfigurationManager.ConnectionStrings["LoggerDb"].ConnectionString);
            LogFunctions.SetLogger(logger);

            ServiceLogContext tmpContext = new ServiceLogContext {
                ServiceName = "Crawler",
                ComponentName = "OnStart",
                RequestId = Guid.Empty,
                SessionId = Guid.Empty
            };

            LogFunctions.Info("Inside WorkerRole.Onstart", tmpContext);

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.

            return base.OnStart();
        }
    }
}
