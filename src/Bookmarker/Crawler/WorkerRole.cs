﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.StorageClient;
using System.Configuration;
using HtmlAgilityPack;
using System.IO;
using BookmarkerDataModel;
using TextAnalysis;
using System.Text.RegularExpressions;

namespace Crawler
{
    public class WorkerRole : RoleEntryPoint
    {
        private DocumentFactory _df;
        private BookmarkerContext _db;
        private int _sleepTime;

        public void Crawl(Bookmark bookmark) {
            string contentType;
            HtmlDocument doc = Download(bookmark.Url, out contentType);
            string title = StripTitle(doc);
            string text = StripText(doc);
            
            bookmark.Title = title != null ? title : bookmark.Title;
            bookmark.ContentType = contentType;            
            bookmark.Content = text;
            bookmark.ContentHash = text.GetHashCode();
            bookmark.LastUpdatedOn = DateTime.UtcNow;
            AddToVocabulary(bookmark);
        }

        private void AddToVocabulary(Bookmark bookmark) {
            Document document = _df.CreateDocument(bookmark.Content);
            foreach (string word in document.Vocabulary) {
                string word1 = word.ToLowerInvariant();
                Vocabulary dbWord = _db.Words.FirstOrDefault(w => w.Word == word1);
                if (dbWord == null) {
                    List<Bookmark> bms = new List<Bookmark>();
                    bms.Add(bookmark);
                    _db.Words.Add(new Vocabulary { Word = word1, Bookmarks = bms });
                }
                else {
                    dbWord.Bookmarks.Add(bookmark);
                }
            }
        }

        private HtmlDocument Download(string bookmarkUrl, out string contentType) {
            HtmlDocument doc = new HtmlDocument();
            WebRequest req = WebRequest.Create(bookmarkUrl);
            using (WebResponse res = req.GetResponse()) {
                contentType = res.ContentType;
                doc.Load(res.GetResponseStream());
            }
            return doc;
        }

        private string StripText(HtmlDocument doc) {
            List<HtmlTextNode> textNodes = new List<HtmlTextNode>();
            GetTextNodes(doc.DocumentNode, textNodes);
            string text = "";
            foreach (HtmlTextNode textNode in textNodes) {
                if (!string.IsNullOrEmpty(textNode.InnerText) && !string.IsNullOrWhiteSpace(textNode.InnerText)) {
                    string tempTxt = textNode.InnerText.Trim();
                    if (Regex.IsMatch(tempTxt, "http://.*") || Regex.IsMatch(tempTxt, "<[^>]+>")) continue;
                    text += textNode.InnerText.Trim() + " ";
                }
            }
            return WebUtility.HtmlDecode(text);
        }

        private string StripTitle(HtmlDocument doc) {
            string title = null;
            HtmlNode titleNode = doc.DocumentNode.SelectSingleNode("/html/head/title");
            if (titleNode != null) {
                title = titleNode.InnerText;
                title = Regex.Replace(title, "\\s+", " ").Trim();
            }
            return title;
        }
        
        public void GetTextNodes(HtmlNode root, List<HtmlTextNode> textNodes) {
            if (root.NodeType == HtmlNodeType.Text) {
                textNodes.Add((HtmlTextNode)root);
            }
            foreach (HtmlNode child in root.ChildNodes) {
                if (child.Name != "script" && child.Name != "style")
                    GetTextNodes(child, textNodes);
            }
        }

        public override void Run() {            
            _db = new BookmarkerContext();
            while ( true ) {
                Trace.WriteLine("Starting crawl");
                List<Bookmark> bookmarks = _db.Bookmarks.ToList();
                foreach (Bookmark bookmark in bookmarks) {
                    try {
                        Trace.WriteLine("Crawling " + bookmark.Url);
                        Crawl(bookmark);
                        _db.SaveChanges();
                    }
                    catch (Exception ex) {
                        Trace.WriteLine("Could not crawl " + bookmark.Url);
                        Trace.WriteLine(ex.GetType().FullName + " " + ex.Message);
                    }
                }                
                Trace.WriteLine("Ending crawl");
                Thread.Sleep(_sleepTime);
            }
        }

        private void InitializeDiagnosticsLogs() {
            var config = DiagnosticMonitor.GetDefaultInitialConfiguration();
            config.Logs.ScheduledTransferPeriod = TimeSpan.FromMinutes(3.0);
            //config.Logs.BufferQuotaInMB = 1024;
            DiagnosticMonitor.Start("Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString", config);
        }

        public override bool OnStart() {
            InitializeDiagnosticsLogs();
            ServicePointManager.DefaultConnectionLimit = 12;
            string crawlTimeSetting = RoleEnvironment.GetConfigurationSettingValue("CrawlIntervalInHours");
            Trace.WriteLine("Crawl time interval has been set as " + crawlTimeSetting);
            _sleepTime = (int)(Double.Parse(crawlTimeSetting) * 60 * 60 * 1000);
            Trace.WriteLine("Setting sleep time as " + _sleepTime + " milliseconds");
            _df = new DocumentFactory();
            return base.OnStart();
        }
    }
}
