using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.StorageClient;
using System.IO;
using System.Text;
using BookmarkerDataModel;
using System.Runtime.Serialization.Json;
using HtmlAgilityPack;

namespace BookmarkerWorker
{
    public class WorkerRole : RoleEntryPoint
    {
        private BookmarkerContext _db = new BookmarkerContext(); // TODO: WILL NOT BE NEEDED
        private CloudStorageAccount _storageAccount;
        private string QUEUE_NAME = "messagequeue";
        private string CONTAINER_NAME = "message-data-container";
        private CloudQueueMessage _msgToCleanup;
        private CloudBlob _blobToCleanup;

        public override void Run() {
            while (true) {
                Thread.Sleep(1000);
                ImportList toImport = PollImporterQueue();
                if (toImport != null) {
                    Trace.WriteLine("Found something in the import queue!");
                    HtmlDocument favorites = DownloadImporterBlob(toImport.BlobName);
                    FavoritesImporter.Import(toImport.UserId, favorites);
                }
                Cleanup();
            }
        }

        private void Cleanup() {
            if (_blobToCleanup != null) {
                _blobToCleanup.Delete();
                _blobToCleanup = null;
            }
            if (_msgToCleanup != null) {
                var queueClient = _storageAccount.CreateCloudQueueClient();
                var queue = queueClient.GetQueueReference(QUEUE_NAME);
                if (queue.Exists()) {
                    queue.DeleteMessage(_msgToCleanup);
                    _msgToCleanup = null;
                }
            }
        }

        private HtmlDocument DownloadImporterBlob(string blobName) {
            CloudBlobClient blobClient = _storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(CONTAINER_NAME);
            CloudBlob blob = container.GetBlobReference(blobName);
            HtmlDocument html = new HtmlDocument();
            using (var ms2 = new MemoryStream()) {
                blob.DownloadToStream(ms2);
                ms2.Position = 0;
                using (StreamReader reader = new StreamReader(ms2)) {
                    html.Load(reader);
                }
            }
            _blobToCleanup = blob;
            return html;
        }

        private ImportList PollImporterQueue() {
            var queueClient = _storageAccount.CreateCloudQueueClient();
            var queue = queueClient.GetQueueReference(QUEUE_NAME);
            if (!queue.Exists()) return null;            
            var msg = queue.GetMessage();
            if (msg == null) return null;
            
            Trace.WriteLine("Processing message in queue with message id " + msg.Id);

            ImportList toImport = ImportList.Deserialize(msg.AsString);
            _msgToCleanup = msg;

            return toImport;            
        }

        private void InitializeDiagnosticsLogs() {
            var config = DiagnosticMonitor.GetDefaultInitialConfiguration();
            config.Logs.ScheduledTransferPeriod = TimeSpan.FromMinutes(3.0);
            //config.Logs.BufferQuotaInMB = 1024;
            DiagnosticMonitor.Start("Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString", config);
        }

        public override bool OnStart() {
            ServicePointManager.DefaultConnectionLimit = 12;
            CloudStorageAccount.SetConfigurationSettingPublisher((configName, configSetter) => {
                configSetter(RoleEnvironment.GetConfigurationSettingValue(configName));
            });
            _storageAccount = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
            InitializeDiagnosticsLogs();
            return base.OnStart();
        }
    }
}
