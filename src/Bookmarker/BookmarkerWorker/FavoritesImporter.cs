﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using BookmarkerDataModel;

namespace BookmarkerWorker
{
    public class FavoritesImporter
    {
        public static void Import(int userId, HtmlDocument favorites) {
            BookmarkerContext _db = new BookmarkerContext();
            var links = favorites.DocumentNode.SelectNodes("//a");
            foreach (var link in links) {
                string href = link.Attributes["href"].Value;
                string name = link.InnerText;
                Bookmark bm = new Bookmark { Url = href };
                Bookmark bm2 = bm.Load(_db);
                if (name.Trim() == "None") {
                    name = bm2.Title;
                }
                var fav = _db.Favorites.FirstOrDefault(f => f.BookmarkId == bm2.Id && f.UserId == userId);
                if (fav == null) {
                    fav = new Favorite {
                        BookmarkId = bm2.Id,
                        CreatedOn = DateTime.UtcNow,
                        IsPinned = false,
                        Name = name,
                        UserId = userId
                    };
                    _db.Favorites.Add(fav);
                }
            }
            _db.SaveChanges();
        }


    }
}
