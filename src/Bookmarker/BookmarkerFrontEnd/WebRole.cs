using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Diagnostics;
using System.Net;

namespace BookmarkerFrontEnd
{
    public class WebRole : RoleEntryPoint
    {
        private static WebRole _instance = null;

        public static WebRole GetInstance() {
            return _instance;
        }

        public Uri GetSearchServiceUrl() {
            //return RoleEnvironment.Roles["SearchService"].Instances[0].InstanceEndpoints["SearchEndPoint"].IPEndpoint;
            RoleInstanceEndpoint ep = RoleEnvironment.Roles["SearchService"].Instances[0].InstanceEndpoints["SearchEndPoint"];
            Uri url = new Uri(ep.Protocol + "://" + ep.IPEndpoint.Address + ":" + ep.IPEndpoint.Port);
            Trace.WriteLine("Returning search service url - " + url.ToString());
            return url;
        }

        private void InitializeDiagnosticsLogs() {
            var config = DiagnosticMonitor.GetDefaultInitialConfiguration();
            config.Logs.ScheduledTransferPeriod = TimeSpan.FromMinutes(3.0);
            //config.Logs.BufferQuotaInMB = 1024;
            DiagnosticMonitor.Start("Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString", config);
        }

        public override bool OnStart() {
            _instance = this;
            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.
            InitializeDiagnosticsLogs();
            foreach (var role in RoleEnvironment.Roles) {
                Trace.WriteLine(role.Key + ": " + role.Value.Name);
                Trace.WriteLine("Role instances :-");
                foreach (var roleInstance in role.Value.Instances) {
                    Trace.WriteLine("Role instance ID: " + roleInstance.Id);
                    Trace.WriteLine("Role instance FD: " + roleInstance.FaultDomain);
                    Trace.WriteLine("Role instance UD: " + roleInstance.UpdateDomain);
                    Trace.WriteLine("Role instance endpoints :-");
                    foreach (var instanceEndpoint in roleInstance.InstanceEndpoints) {
                        Trace.WriteLine(instanceEndpoint.Key + ": " + instanceEndpoint.Value.IPEndpoint.ToString());
                    }
                }
            }
            return base.OnStart();
        }


    }
}
