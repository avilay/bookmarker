﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using BookmarkerFrontEnd.Facebook;

namespace BookmarkerFrontEnd.Controllers
{
    [P3P]
    public class AccountController : Controller
    {
        private Facebook.OAuthClient _fc;

        public AccountController() {
            _fc = new Facebook.OAuthClient(this, new string[] { });
        }

        public ActionResult LogOn() {
            return _fc.StartOAuth();
        }

        public ActionResult LogOff() {
            return _fc.LogOff();
        }

        public ActionResult WebsiteLogOn(string code) {
            return _fc.WebsiteLogOn(code);
        }

        public ActionResult CanvasLogOn(string signed_request) {
            if (Request["next"] != null && Request["next"] == "claim") {
                if (Request["memento"] != null && Request["nonce"] != null) {
                    string returnUrl = string.Format("/Mementos/Claim?mementoId={0}&nonce={1}",
                        Request["memento"], Request["nonce"]);
                    Session["ReturnUrl"] = returnUrl;
                }
            }
            return _fc.CanvasLogOn(signed_request);
        }

        public ActionResult LoginFail() {
            return View();
        }        



        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus) {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch ( createStatus ) {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
