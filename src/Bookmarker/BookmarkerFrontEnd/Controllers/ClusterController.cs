﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookmarkerDataModel;
using System.Web.Configuration;

namespace BookmarkerFrontEnd.Controllers
{ 
    public class ClusterController : Controller
    {
        private BookmarkerContext _db = new BookmarkerContext();
        private int _dbugUserId;

        public void Initialize() {
            _dbugUserId = 1;
            GetAndSetPinnedItems();
            ViewBag.SearchParams = "";
            ViewBag.GroupsSelected = "selected";
        }

        public ViewResult Index() {
            Initialize();
            var clusters = from c in _db.Clusters                           
                           where c.UserId == _dbugUserId
                           select c;
            return View(clusters.Distinct().ToList());
        }

        public ViewResult Details(int id) {
            Initialize();
            Cluster cluster = _db.Clusters.Include(c => c.Favorites).Where(c => c.Id == id).First();            
            return View(cluster);
        }

        public ActionResult Create() {
            Initialize();
            //ViewBag.FreeFavorites = new SelectList(_db.Favorites.Where(f => f.UserId == _dbgUserId && f.ClusterId == null), "Id", "Name");
            //ViewBag.ClusteredFavorites = new SelectList(_db.Favorites.Where(f => f.UserId == _dbgUserId && f.ClusterId != null), "Id", "Name");            
            return View();
        } 

        [HttpPost]
        public ActionResult Create(Cluster cluster) {
            Initialize();
            cluster.CreatedOn = DateTime.Now;
            
            if (ModelState.IsValid) {
                cluster.UserId = _dbugUserId;
                _db.Clusters.Add(cluster);
                _db.SaveChanges();                
                return RedirectToAction("Index");  
            }
            //ViewBag.FreeFavorites = new SelectList(_db.Favorites.Where(f => f.UserId == _dbgUserId && f.ClusterId == null), "Id", "Name");
            //ViewBag.ClusteredFavorites = new SelectList(_db.Favorites.Where(f => f.UserId == _dbgUserId && f.ClusterId != null), "Id", "Name");
            return View(cluster);
        }
        
        public ActionResult Edit(int id) {
            Initialize();
            Cluster cluster = _db.Clusters.Find(id);
            return View(cluster);
        }

        [HttpPost]
        public ActionResult Edit(Cluster cluster) {
            Initialize();
            if (ModelState.IsValid) {
                _db.Entry(cluster).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cluster);
        }

        public ActionResult Delete(int id) {
            Initialize();
            Cluster cluster = _db.Clusters.Find(id);
            return View(cluster);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id) {
            Initialize();
            var favs = from f in _db.Favorites
                       where f.ClusterId == id
                       select f;
            foreach ( Favorite fav in favs ) {
                fav.ClusterId = null;
            }
            _db.SaveChanges();

            Cluster cluster = _db.Clusters.Find(id);
            _db.Clusters.Remove(cluster);
            _db.SaveChanges();

            return RedirectToAction("Index");
        }

        private void GetAndSetPinnedItems() {
            var favorites = from f in _db.Favorites.Include(f => f.Bookmark).Include(f => f.User).Include(f => f.Cluster)
                            where f.UserId == 1 && f.IsPinned == true
                            select f;
            ViewData["Pinned"] = favorites.ToList();
        }

        protected override void Dispose(bool disposing) {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}