﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookmarkerDataModel;

namespace BookmarkerFrontEnd.Controllers
{ 
    public class ToolsController : Controller
    {
        private BookmarkerContext db = new BookmarkerContext();





        //
        // GET: /Tools/

        public ViewResult Index()
        {
            var favorites = db.Favorites.Include(f => f.Bookmark).Include(f => f.User).Include(f => f.Cluster);
            return View(favorites.ToList());
        }

        //
        // GET: /Tools/Details/5

        public ViewResult Details(int id)
        {
            Favorite favorite = db.Favorites.Find(id);
            return View(favorite);
        }

        //
        // GET: /Tools/Create

        public ActionResult Create()
        {
            ViewBag.BookmarkId = new SelectList(db.Bookmarks, "Id", "Title");
            ViewBag.UserId = new SelectList(db.Users, "Id", "Name");
            ViewBag.ClusterId = new SelectList(db.Clusters, "Id", "Name");
            return View();
        } 

        //
        // POST: /Tools/Create

        [HttpPost]
        public ActionResult Create(Favorite favorite)
        {
            if (ModelState.IsValid)
            {
                db.Favorites.Add(favorite);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.BookmarkId = new SelectList(db.Bookmarks, "Id", "Title", favorite.BookmarkId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Name", favorite.UserId);
            ViewBag.ClusterId = new SelectList(db.Clusters, "Id", "Name", favorite.ClusterId);
            return View(favorite);
        }
        
        //
        // GET: /Tools/Edit/5
 
        public ActionResult Edit(int id)
        {
            Favorite favorite = db.Favorites.Find(id);
            ViewBag.BookmarkId = new SelectList(db.Bookmarks, "Id", "Title", favorite.BookmarkId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Name", favorite.UserId);
            ViewBag.ClusterId = new SelectList(db.Clusters, "Id", "Name", favorite.ClusterId);
            return View(favorite);
        }

        //
        // POST: /Tools/Edit/5

        [HttpPost]
        public ActionResult Edit(Favorite favorite)
        {
            if (ModelState.IsValid)
            {
                db.Entry(favorite).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BookmarkId = new SelectList(db.Bookmarks, "Id", "Title", favorite.BookmarkId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Name", favorite.UserId);
            ViewBag.ClusterId = new SelectList(db.Clusters, "Id", "Name", favorite.ClusterId);
            return View(favorite);
        }

        //
        // GET: /Tools/Delete/5
 
        public ActionResult Delete(int id)
        {
            Favorite favorite = db.Favorites.Find(id);
            return View(favorite);
        }

        //
        // POST: /Tools/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Favorite favorite = db.Favorites.Find(id);
            db.Favorites.Remove(favorite);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}