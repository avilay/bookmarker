﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookmarkerDataModel;
using System.Web.Configuration;
using System.Web.Routing;
using BookmarkerFrontEnd.Facebook;

namespace BookmarkerFrontEnd.Controllers
{ 
    public class PinnedController : Controller
    {
        private BookmarkerContext _db = new BookmarkerContext();
        private int _userId;

        // Why is this code not in the ctor?
        // Because the Session object is still null when the ctor is called.
        // Even though the Session_Start has been called before it.
        public void Initialize() {
            string fbUserId = Session["UserId"] as string;
            string fbUserName = Session["UserName"] as string;
            _userId = FacebookUser.FindOrCreate(_db, fbUserId, fbUserName).Id;
            _db = new BookmarkerContext();
            GetAndSetPinnedItems();
            ViewBag.PinnedSelected = "selected";
        }

        private void GetAndSetPinnedItems() {
            var favorites = from f in _db.Favorites.Include(f => f.Bookmark).Include(f => f.User).Include(f => f.Cluster)
                            where f.UserId == _userId && f.IsPinned == true
                            select f;
            ViewData["Pinned"] = favorites.ToList();
        }

        public ActionResult Add(int id) {
            Initialize();
            Favorite toPin = _db.Favorites.FirstOrDefault(f => f.Id == id && f.UserId == _userId && f.IsPinned == false);
            if (toPin != null) {
                toPin.IsPinned = true;
                _db.SaveChanges();
            }           
            return RedirectToAction("Details", "Favorite", new RouteValueDictionary { { "id", id } });            
        }
        
        public ActionResult Edit() {
            Initialize();
            var favorites = from f in _db.Favorites.Include(f => f.Bookmark).Include(f => f.User).Include(f => f.Cluster)
                            where f.UserId == _userId && f.IsPinned == true
                            select f;
            return View(favorites.ToList());
        }

        [HttpPost]
        public ActionResult Edit(FormCollection coll) {
            Initialize();
            List<int> ids = new List<int>();
            foreach ( string key in coll.Keys ) {
                if ( coll[key].Contains("true") ) {
                    ids.Add(Int32.Parse(key));
                }
            }
            var favorites = from f in _db.Favorites.Include(f => f.Bookmark).Include(f => f.User).Include(f => f.Cluster)
                            where f.UserId == _userId
                            && f.IsPinned == true
                            && ids.Contains(f.Id)
                            select f;
            foreach ( Favorite favorite in favorites ) {
                favorite.IsPinned = false;
            }
            _db.SaveChanges();
            return RedirectToAction("Index", "Favorite");
        }
        
        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}