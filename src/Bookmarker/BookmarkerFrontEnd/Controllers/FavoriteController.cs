﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookmarkerDataModel;
using System.Web.Configuration;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Diagnostics;
using System.Net;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using BookmarkerFrontEnd.Facebook;

namespace BookmarkerFrontEnd.Controllers
{ 
    [Authorize]
    public class FavoriteController : Controller
    {
        private BookmarkerContext _db = new BookmarkerContext();
        private int _userId;
        private string QUEUE_NAME = "messagequeue";
        private string CONTAINER_NAME = "message-data-container";
        private CloudStorageAccount _storageAccount;

        // See HomeController.Initialize
        public void Initialize() {            
            string fbUserId = Session["UserId"] as string;
            string fbUserName = Session["UserName"] as string;
            _userId = FacebookUser.FindOrCreate(_db, fbUserId, fbUserName).Id;
            //_dbugUserId = 1;
            GetAndSetPinnedItems();
            ViewBag.SearchParams = "";
            ViewBag.BookmarksSelected = "selected";
            _storageAccount = CloudStorageAccount.FromConfigurationSetting("DataConnectionString");
        }

        private void GetAndSetPinnedItems() {
            var favorites = from f in _db.Favorites.Include(f => f.Bookmark).Include(f => f.User).Include(f => f.Cluster)
                            where f.UserId == _userId && f.IsPinned == true
                            select f;
            ViewData["Pinned"] = favorites.ToList();
        }

        public ViewResult Index() {
            Initialize();
            Trace.WriteLine("Inside FavoritesController.Index");
            var favorites = from f in _db.Favorites.Include(f => f.Bookmark).Include(f => f.User).Include(f => f.Cluster)
                            where f.UserId == _userId
                            select f;
            ViewBag.PageName = "Bookmarks";
            return View(favorites.OrderByDescending(f => f.CreatedOn).ToList());
        }

        public ViewResult Import() {
            Initialize();            
            return View();
        }

        private void CreateBlob(string blobName, HttpPostedFileBase file) {
            CloudBlobClient blobClient = _storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(CONTAINER_NAME);
            container.CreateIfNotExist();
            CloudBlob blob = container.GetBlobReference(blobName);
            using (var fileStream = file.InputStream) {
                blob.UploadFromStream(fileStream);
            }
        }

        private void QueueImportList(ImportList toImport) {
            var queueClient = _storageAccount.CreateCloudQueueClient();
            var queue = queueClient.GetQueueReference(QUEUE_NAME);
            queue.CreateIfNotExist();
                        
            string importList = toImport.ToJsonString();
            var msg = new CloudQueueMessage(importList);
            queue.AddMessage(msg);
        }

        [HttpPost]
        public ViewResult Import(HttpPostedFileBase file) {
            Initialize();
            string blobName = Guid.NewGuid().ToString();
            CreateBlob(blobName, file);
            ImportList toImport = new ImportList { UserId = _userId, BlobName = blobName };
            QueueImportList(toImport);
            ViewBag.Notice = "Your bookmarks have been queued up for import. Check back in a few minutes.";
            return View();
        }

        public ActionResult Search(string keywords) {
            Initialize();
            Uri searchSvc = BuildSearchSvcUrl(keywords);
            string content = "";
            using (WebClient client = new WebClient()) {
                content = client.DownloadString(searchSvc);
            }
            List<BookmarkInfo> searchResults;
            using ( var ms = new MemoryStream(Encoding.Unicode.GetBytes(content)) ) {
                var serialiser = new DataContractJsonSerializer(typeof(List<BookmarkInfo>));
                searchResults = (List<BookmarkInfo>)serialiser.ReadObject(ms);
            }
            ViewBag.PageName = "Search results for " + keywords;
            ViewBag.SearchParams = keywords;
            return View(searchResults.OrderBy(bi => bi.Relevance));
        }

        private Uri BuildSearchSvcUrl(string keywords) {
            RoleInstanceEndpoint ep = RoleEnvironment.Roles["SearchService"].Instances[0].InstanceEndpoints["SearchEndPoint"];
            Uri searchSvcBase = new Uri(ep.Protocol + "://" + ep.IPEndpoint.Address + ":" + ep.IPEndpoint.Port);
            string searchRel = string.Format("Search/?userId={0}&query={1}", _userId, keywords);
            Uri searchSvc = new Uri(searchSvcBase, searchRel);
            return searchSvc;
        }

        public ViewResult Details(int id) {
            Initialize();
            Favorite favorite = _db.Favorites.Find(id);
            return View(favorite);
        }

        public ActionResult CreateFirstPage() {
            Initialize();
            return View();
        }
        
        public ActionResult CreateSecondPage(Bookmark bookmark, string popup) {
            Initialize();
            if ( ModelState.IsValid ) {
                //bookmark.SetLogContext(GetServiceLogContext());
                Bookmark bookmark2 = bookmark.Load(_db);
                var favs = _db.Favorites.Where(f => f.BookmarkId == bookmark2.Id && f.UserId == _userId);
                if ( favs.Count() == 1 ) {
                    return RedirectToAction("Edit", new { id = favs.First().Id });
                }
                Favorite favorite = new Favorite();
                favorite.BookmarkId = bookmark2.Id;
                favorite.Name = bookmark2.Title;
                ViewBag.BookmarkUrl = bookmark2.Url;
                ViewBag.ClusterId = new SelectList(_db.Clusters, "Id", "Name");
                if ( popup == "true" ) {
                    //return View("CreatePopup");
                    return View("CreatePopup", favorite);
                }
                else {
                    return View(favorite);
                }
            }
            return RedirectToAction("CreateFirstPage");                        
        }
                
        public ActionResult CreateEnd(Favorite favorite) {
            Initialize();
            if ( ModelState.IsValid ) {
                if ( string.IsNullOrEmpty(favorite.Name) ) {
                    favorite.Name = _db.Bookmarks.Find(favorite.BookmarkId).Url;
                }
                favorite.UserId = _userId;
                favorite.CreatedOn = DateTime.Now;
                _db.Favorites.Add(favorite);
                _db.SaveChanges();
                if ( Request.UrlReferrer.Query.EndsWith("popup=true") ) {
                    return View("EndPopup");
                }
                else {
                    return RedirectToAction("Index");
                }
            }

            // TODO: Get clusters belonging to only this user, not all users.
            ViewBag.ClusterId = new SelectList(_db.Clusters, "Id", "Name", favorite.ClusterId);
            return View(favorite);            
        }
        
        public ActionResult Edit(int id) {
            Initialize();
            Favorite favorite = _db.Favorites.Find(id);
            ViewBag.ClusterId = new SelectList(_db.Clusters, "Id", "Name", favorite.ClusterId);
            return View(favorite);
        }

        [HttpPost]
        public ActionResult Edit(Favorite favorite) {
            //Initialize();
            ViewData["Pinned"] = new List<Favorite>();
            _userId = 1;
            if (ModelState.IsValid)
            {
                favorite.UserId = _userId;
                _db.Entry(favorite).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }            
            ViewBag.ClusterId = new SelectList(_db.Clusters, "Id", "Name", favorite.ClusterId);
            GetAndSetPinnedItems();
            return View(favorite);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id) {
            Initialize();
            Favorite favorite = _db.Favorites.Find(id);
            _db.Favorites.Remove(favorite);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing) {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}