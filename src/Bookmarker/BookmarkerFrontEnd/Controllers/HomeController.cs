﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookmarkerDataModel;
using System.Web.Configuration;
using BookmarkerFrontEnd.Facebook;

namespace BookmarkerFrontEnd.Controllers
{
    public class HomeController : Controller
    {
        private BookmarkerContext _db = new BookmarkerContext();
        private int _userId;
        
        // Why is this code not in the ctor?
        // Because the Session object is still null when the ctor is called.
        // Even though the Session_Start has been called before it.
        public void Initialize() {
            if (Session["UserId"] != null && Session["UserName"] != null) {
                string fbUserId = Session["UserId"] as string;
                string fbUserName = Session["UserName"] as string;
                _userId = FacebookUser.FindOrCreate(_db, fbUserId, fbUserName).Id;
            }
            else {
                _userId = 0;
            }
            _db = new BookmarkerContext();            
            GetAndSetPinnedItems();
            ViewBag.HomeSelected = "selected";
        }

        private void GetAndSetPinnedItems() {
            var favorites = from f in _db.Favorites.Include(f => f.Bookmark).Include(f => f.User).Include(f => f.Cluster)
                            where f.UserId == _userId && f.IsPinned == true
                            select f;
            ViewData["Pinned"] = favorites.ToList();
        }

        public ActionResult Index() {
            Initialize();
            return View();
        }

        public ActionResult About() {
            Initialize();
            return View();
        }

        public ActionResult Contact() {
            Initialize();
            return View();
        }

        public ActionResult Tools() {
            Initialize();
            ViewBag.HomeSelected = "";
            ViewBag.ToolsSelected = "selected";
            return View();
        }
    }
}
