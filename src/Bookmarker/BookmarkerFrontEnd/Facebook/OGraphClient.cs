﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Web.Mvc;
using Jayrock.Json;
using Jayrock.Json.Conversion;

namespace BookmarkerFrontEnd.Facebook
{
    public class OGraphClient
    {
        private string _accessToken;
        private Controller _caller;

        public OGraphClient(Controller caller) {
            _caller = caller;
        }

        // Cannot call this from the ctor because the ctor could've been called from controller.ctor whic
        // does not have the session object initialized
        private void SetAccessToken() {
            _accessToken = _caller.Session["AccessToken"] as string;
        }

        // TODO: Handle exceptions
        public void PostMessage(string message) {
            SetAccessToken();
            if (_accessToken == null) {
                throw new InvalidOperationException("Cannot post message without a valid access token");
            }
            string url = "https://graph.facebook.com/me/feed";
            HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
            req.Method = "POST";
            string postMsg = "access_token=" + _accessToken;
            postMsg += "&message=" + message;
            req.ContentLength = postMsg.Length;
            using (StreamWriter writer = new StreamWriter(req.GetRequestStream())) {
                writer.Write(postMsg);
            }
            using (HttpWebResponse res = req.GetResponse() as HttpWebResponse) {
                using (StreamReader reader = new StreamReader(res.GetResponseStream())) {
                    string content = reader.ReadToEnd();
                }
            }
        }

        public void PostLink(string link, string message, string description, string picture) {
            SetAccessToken();
            if (_accessToken == null) {
                throw new InvalidOperationException("Cannot post message without a valid access token");
            }
            string url = "https://graph.facebook.com/me/feed";
            HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
            req.Method = "POST";
            string postMsg = "access_token=" + _accessToken;
            postMsg += "&link=" + link;
            postMsg += "&message=" + message;
            postMsg += "&description=" + description;
            postMsg += "&picture=" + picture;
            req.ContentLength = postMsg.Length;
            using (StreamWriter writer = new StreamWriter(req.GetRequestStream())) {
                writer.Write(postMsg);
            }
            using (HttpWebResponse res = req.GetResponse() as HttpWebResponse) {
                using (StreamReader reader = new StreamReader(res.GetResponseStream())) {
                    string content = reader.ReadToEnd();
                }
            }
        }

        // TODO: Returns only the first 5000 friends
        public List<FacebookUser> GetFriends() {
            SetAccessToken();
            if (_accessToken == null) {
                throw new InvalidOperationException("Cannot get friends without a valid access token");
            }
            string url = "https://graph.facebook.com/me/friends?access_token={0}&limit=5000&offset=0";
            url = string.Format(url, _accessToken);
            HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
            string content = "";
            try {
                using (HttpWebResponse res = req.GetResponse() as HttpWebResponse) {
                    using (StreamReader reader = new StreamReader(res.GetResponseStream())) {
                        content = reader.ReadToEnd();
                    }
                }
                JsonObject jo = JsonConvert.Import(content) as JsonObject;
                JsonArray data = jo["data"] as JsonArray;
                string sdata = data.ToString();
                FacebookUser[] users = JsonConvert.Import(typeof(FacebookUser[]), sdata) as FacebookUser[];
                return users.ToList();
            }
            catch (WebException we) {
                throw;
            }
        }

    }

}