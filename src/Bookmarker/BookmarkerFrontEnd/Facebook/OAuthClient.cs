﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using System.Web.Security;
using System.Net;
using System.IO;
using Jayrock.Json;
using Jayrock.Json.Conversion;
using System.Text;

namespace BookmarkerFrontEnd.Facebook
{
    public class OAuthClient
    {
        private static string OAUTH_TEMPLATE =
            "https://www.facebook.com/dialog/oauth?client_id={0}&redirect_uri={1}";
        private static string GRAPH_TEMPLATE = "https://graph.facebook.com/oauth/access_token?client_id={0}&" +
                "redirect_uri={1}&client_secret={2}&code={3}";
        private static string ME_TEMPLATE = "https://graph.facebook.com/me?access_token={0}";
        private static string LOGOFF_TEMPLATE = "https://www.facebook.com/logout.php?next={0}&access_token={1}";
        private static string PERMISSIONS_TEMPLATE = "https://graph.facebook.com/me/permissions?access_token={0}";

        private string _appId;
        private string _appSecret;
        private string _logOffUrl;
        private string _redirectUrl;
        private string _loginFailUrl;
        private string _loginPassUrl;
        private string _code;
        private string _accessToken;

        private string[] _permissions;

        private Controller _caller;

        public OAuthClient(Controller caller, string[] permissions = null, bool isCanvas = false) {
            _caller = caller;
            _appId = WebConfigurationManager.AppSettings["appId"];
            _appSecret = WebConfigurationManager.AppSettings["appSecret"];
            string hostName = WebConfigurationManager.AppSettings["hostName"];
            _logOffUrl = hostName + "/Home/Index";
            if (isCanvas) {
                _redirectUrl = hostName + "/Account/CanvasLogOn";
            }
            else {
                _redirectUrl = hostName + "/Account/WebsiteLogOn";
            }
            _loginFailUrl = hostName + "/Account/LoginFail";
            _loginPassUrl = hostName + "/Home/Index";
            _permissions = permissions;
        }

        // Call this from Account.LogOn
        public ActionResult StartOAuth() {
            string oauth = OAuthEndpoint;
            if (_permissions != null) {
                oauth += "&scope=";
                foreach (string permission in _permissions) {
                    oauth += permission + ",";
                }
                oauth = oauth.Trim(',');
            }
            return new RedirectResult(oauth);
        }

        // Call this from Account.CanvasLogOn. Only needed if this is a canvas
        public ActionResult CanvasLogOn(string signed_request) {
            return Authenticate(signed_request, true);
        }

        // Call this from Account.WebsiteLogOn. Only needed if this is a website
        public ActionResult WebsiteLogOn(string code) {
            return Authenticate(code, false);
        }

        // Call this from Account.LogOff
        public ActionResult LogOff() {
            FormsAuthentication.SignOut();
            _accessToken = _caller.Session["AccessToken"] as string;
            return new RedirectResult(LogoffEndpoint);
        }

        private ActionResult Authenticate(string param, bool isCanvas) {
            try {
                if (isCanvas) {
                    SetAccessTokenFromSignedRequest(param);
                }
                else {
                    _code = param;
                    SetAccessTokenFromCode();
                }
                if (!string.IsNullOrEmpty(_accessToken) && CheckPermissions(GetPermissions())) {
                    SignInUser(GetFacebookUser());
                    if (_caller.Session["ReturnUrl"] != null) {
                        return new RedirectResult((string)_caller.Session["ReturnUrl"]);
                    }
                    return new RedirectResult(_loginPassUrl);
                }
                else {
                    return StartOAuth();
                }
            }
            catch (Exception e) {
                return new RedirectResult(_loginFailUrl);
            }
        }

        private bool CheckPermissions(JsonObject perms) {
            foreach (string perm in _permissions) {
                if (perms.Contains(perm)) {
                    JsonNumber val = (JsonNumber)perms[perm];
                    int vali = val.ToInt32();
                    if (vali == 1) {
                        continue;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }
            return true;
        }

        private JsonObject GetPermissions() {
            HttpWebRequest req = WebRequest.Create(PermissionsEndpoint) as HttpWebRequest;
            string content = "";
            try {
                using (HttpWebResponse res = req.GetResponse() as HttpWebResponse) {
                    using (StreamReader reader = new StreamReader(res.GetResponseStream())) {
                        content = reader.ReadToEnd();
                    }
                }
                JsonObject jo = JsonConvert.Import(content) as JsonObject;
                JsonArray data = jo["data"] as JsonArray;
                JsonObject perms = data[0] as JsonObject;
                return perms;
            }
            catch (WebException we) {
                throw;
            }
        }

        private FacebookUser GetFacebookUser() {
            string content = "";
            HttpWebRequest req = WebRequest.Create(MeEndpoint) as HttpWebRequest;
            try {
                using (HttpWebResponse res = req.GetResponse() as HttpWebResponse) {
                    using (StreamReader reader = new StreamReader(res.GetResponseStream())) {
                        content = reader.ReadToEnd();
                    }
                }
            }
            catch (WebException we) {
                throw;
            }
            FacebookUser user = JsonConvert.Import(typeof(FacebookUser), content) as FacebookUser;
            return user;
        }

        private void SignInUser(FacebookUser fbUser) {
            _caller.Session["UserName"] = fbUser.name;
            _caller.Session["UserId"] = fbUser.id;
            FormsAuthentication.SetAuthCookie(fbUser.name, false);
        }

        private void SetAccessTokenFromSignedRequest(string signed_request) {
            string b64json = signed_request.Split('.')[1];
            b64json = b64json.Replace('-', '+').Replace('_', '/');
            int len = b64json.Length % 4;
            if (len > 0) b64json = b64json.PadRight(b64json.Length + (4 - len), '=');
            byte[] jsonBytes = Convert.FromBase64String(b64json);
            string json = Encoding.UTF8.GetString(jsonBytes);
            //SignedRequest sr = JsonConvert.Import(typeof(SignedRequest), json) as SignedRequest;
            JsonObject jsonObj = JsonConvert.Import(json) as JsonObject;
            _accessToken = jsonObj["oauth_token"] as string;
            _caller.Session["AccessToken"] = _accessToken;
        }

        private void SetAccessTokenFromCode() {
            string content = "";
            HttpWebRequest req = WebRequest.Create(GraphEndpoint) as HttpWebRequest;
            try {
                using (HttpWebResponse res = req.GetResponse() as HttpWebResponse) {
                    using (StreamReader reader = new StreamReader(res.GetResponseStream())) {
                        content = reader.ReadToEnd();
                    }
                    _accessToken = content.Split('&')[0].Substring("access_token=".Length);
                    _caller.Session["AccessToken"] = _accessToken;
                }
            }
            catch (WebException we) {
                throw;
            }
        }

        public string OAuthEndpoint {
            get {
                if (string.IsNullOrEmpty(_appId) || string.IsNullOrEmpty(_redirectUrl)) {
                    throw new ArgumentException("Need application id and redirect url for building OAuthEndpoint");
                }
                return string.Format(OAUTH_TEMPLATE, _appId, _redirectUrl);
            }
        }

        public string GraphEndpoint {
            get {
                if (string.IsNullOrEmpty(_appId) || string.IsNullOrEmpty(_redirectUrl) || string.IsNullOrEmpty(_appSecret) || string.IsNullOrEmpty(_code)) {
                    throw new ArgumentException("GraphEndpoint cannot be constructed");
                }
                return string.Format(GRAPH_TEMPLATE, _appId, _redirectUrl, _appSecret, _code);

            }
        }

        public string MeEndpoint {
            get {
                if (string.IsNullOrEmpty(_accessToken)) {
                    throw new ArgumentException("Need acess toke to build MeEndpoint");
                }
                return string.Format(ME_TEMPLATE, _accessToken);
            }
        }

        public string LogoffEndpoint {
            get {
                if (string.IsNullOrEmpty(_logOffUrl) || string.IsNullOrEmpty(_accessToken)) {
                    throw new ArgumentException("Cannot build LogoffEndpoint");
                }
                return string.Format(LOGOFF_TEMPLATE, _logOffUrl, _accessToken);
            }
        }

        public string PermissionsEndpoint {
            get {
                if (string.IsNullOrEmpty(_accessToken)) {
                    throw new ArgumentException("Need access token to get permissions");
                }
                return string.Format(PERMISSIONS_TEMPLATE, _accessToken);
            }
        }

    }

}