﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BookmarkerDataModel;
using User = BookmarkerDataModel.User;

namespace BookmarkerFrontEnd.Facebook
{
    public class FacebookUser
    {
        public string id { get; set; }
        public string name { get; set; }

        public static User FindOrCreate(BookmarkerContext db, string facebookId, string name) {
            var vuser = db.Users.Where(u => u.FacebookId == facebookId);
            if (vuser.Count() == 0) {
                User usr = new User { 
                    FacebookId = facebookId, 
                    LastActivityOn = DateTime.Now, 
                    Name = name, 
                    Expires = DateTime.Now 
                };
                db.Users.Add(usr);
                db.SaveChanges();
                return usr;
            }
            else if (vuser.Count() == 1) {
                return vuser.First();
            }
            else {
                throw new Exception("there are two users with the same facebook Id " + facebookId);
            }
        }
    }
}